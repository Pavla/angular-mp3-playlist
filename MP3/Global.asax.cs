﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MP3
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            addData();
            
        }

        private void addData() {

            MP3DatabaseEntities1 db = new MP3DatabaseEntities1();
            db.Database.ExecuteSqlCommand("DELETE FROM Music;");
            db.Database.ExecuteSqlCommand("DELETE FROM PlayList;");
            db.Database.ExecuteSqlCommand("DELETE FROM ManageList;");

            db.Music.Add(new Music("Adele", "Hello", "", "Soul and R&B"));
            db.Music.Add(new Music("Anastacia", "Paid my dues", "Hit Singles", "Pop"));
            db.Music.Add(new Music("Billy Idol", "Sweet Sixteen", "Single", "Noise"));
            db.Music.Add(new Music("Boney M", "Disco Megamix", "Boney M. 2000", "Disco"));
            db.Music.Add(new Music("Children Of Bodom", "Children Of Bodom", "Something Wild", "Black Metal"));
            db.Music.Add(new Music("Coldplay", "Viva La Vida", "Viva La Vida", "Alternative"));
            db.Music.Add(new Music("Era", "Divano", "The Very Best of Era", "Dance"));
            db.Music.Add(new Music("Sade", "Soldier Of Love", "The Ultimate Collection Disc 2", "Soul and R&B"));
            db.Music.Add(new Music("Enya", "Hope Has A Place", "The Memory Of Trees", "New Age"));
            db.Music.Add(new Music("Enya ", "AnywhereIs", "The Memory Of Trees", "New Age"));
            db.Music.Add(new Music("Reamonn", "Supergirl ", "Supergirl Maxi", ""));
            db.Music.Add(new Music("Robbie Williams ", "Supreme", "Sing When You're Winning", "Pop"));
            db.Music.Add(new Music("Moby", "Honey", "Play", "Electronic"));
            db.Music.Add(new Music("Michael Jackson", "Earth Song", "King of Pop", "Soul and R&B"));
            db.Music.Add(new Music("Pink ", "Dear Mr. President", "I'm Not Dead", "Other"));
            db.Music.Add(new Music("Eros Ramazzotti ", "Fuoco del fuoco", "Fuoco del fuoco", "Other"));
            db.Music.Add(new Music("Guns N' Roses", "Don't cry", "Use your illusion 2", "Rock"));
            db.Music.Add(new Music("The Rolling Stones", "Angie", "The best of Rolling Stones", "Rock"));
            db.Music.Add(new Music("Korn", "Coming Undone", "See You On The Other Side", "Hard Rock"));
            db.Music.Add(new Music("Ozzy Osbourne", "No More Tears", "No More Tears", "Blues"));
            db.Music.Add(new Music("Kid Cudi Vs Crookers", "Day N Nite", "MOS Presents Addicted To Bass 2009", "Bass"));
            db.Music.Add(new Music("Eiffel 65", "The Bad Touch", "", "Latino Dance"));
            db.Music.Add(new Music("DJ Tiesto", "Insomnia", "Insomnia", "Techno/Trance"));
            db.Music.Add(new Music("September", "Because I love you", "Bravo Hits Zima 2009", "Top 40"));


            db.SaveChanges();

        }


    }
}