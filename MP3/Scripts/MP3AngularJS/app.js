﻿var myapp = angular.module('MP3App', ['ui.bootstrap', 'ui.router']);

myapp.run(function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;             // for ng-class={active: $state.includes()};
    $rootScope.$stateParams = $stateParams;
});


myapp.config(function ($stateProvider, $urlRouterProvider) {

    //$urlRouterProvider.when("", "/music");
    //$urlRouterProvider.when("/", "/music");
    
    $urlRouterProvider.otherwise("/");

    $stateProvider
            .state('home', {
                url: '/',
            })

            .state('music', {
                url: '/music',
                templateUrl: 'Templates/MP3List.html',
                controller: 'MP3ListController'
            })

            .state('playlist', {
                url: '/playlist',
                templateUrl: 'Templates/PlayList.html',
                controller: 'PlayListController'
            })

            .state('playlist.details', {
                parent: "playlist",
                url: "/:id",
                templateUrl: "Templates/PlayList.details.html",
                controller: 'EditPlayListNameController'
            })
    ;
});



