﻿myapp.controller('MP3ListController', ['$scope', '$http', '$stateParams', '$modal', '$location', function ($scope, $http, $stateParams, $modal, $location) {

    $scope.MusicList = {};
    
    //GET
    $http({
        method: 'GET',
        url: "/Data/GetMP3List",
    })
      .success(function (data) {
          $scope.MusicList = data;
      })
      .error(function (data) {
          alertify.alert("Error getting MP3 list")
              .set({ transition: 'fade' })
              .set({ title: '<span class="fa fa-times-circle fa-2x" ' + 'style="vertical-align:middle;color:#e10000;">' + '</span> Error' })

      });


    //DELETE
    $scope.deleteMusic = function (id) {

        alertify.dialog('confirm')
           .show()
           .set({ title: 'Delete music' })                   
           .set('defaultFocus', 'cancel')  
           .set({
               'labels': { ok: 'Ok', cancel: 'Cancel' },
               'message': 'Are you sure you want to delete?',
               'onok': function () {

                   $http({
                       method: 'DELETE',
                       url: '/Data/DeleteMP3/' + id,
                   })

                   .success(function (data) {
                       for (var i = 0; i < $scope.MusicList.length; i++) {
                           if ($scope.MusicList[i].MusicID === id) {
                               $scope.MusicList.splice(i, 1);
                           }
                       }
                   })
                   .error(function (data) {
                       alertify.error("Error with number id =" + id);
                            
                   });

               },
               'oncancel': function () {  }
           });
    };

    //MODAL ADD
    $scope.AddMusicModal = function () {
        var modalInstance = $modal.open({
            templateUrl: 'Templates/AddMusicModal.html',
            controller: 'AddMusicModalController',
            resolve: {
                test: function () {                   //pass parameter to controller
                    return $scope.MusicList;
                },
            }
        });
    }

    //MODAL EDIT
    $scope.EditMusicModal = function (music) {

        var modalInstance = $modal.open({
            templateUrl: 'Templates/EditMusicModal.html',
            controller: 'EditMusicModalController',
            resolve: {
                selectedMusic: function () {
                    return music;
                },

                AllMusic: function () {                 
                    return $scope.MusicList;
                },
            }
        });

    }

    

    //SEARCH
    $scope.SearchMusic = function () {

        if ($scope.data.singleSelectMusic != "") {

            $http({
                method: 'GET',
                url: "/Data/FilterMP3List",
                params: { singleSelectMusic: $scope.data.singleSelectMusic, inputSearchMusic: $scope.input.SearchMusic }
            })
              .success(function (data) {
                  $scope.MusicList = data;
              })
              .error(function (data) {

                  alertify.error("Error with filtering MP3");

                });
        }
    }

    //CHECKBOX 
    $scope.allItemsSelected = false;

    $scope.selectMusicCheckbox = function () {

        for (var i = 0; i < $scope.MusicList.length; i++) {
            if (!$scope.MusicList[i].isChecked) {
                $scope.allItemsSelected = false;
                return;
            }
        }
        $scope.allItemsSelected = true;
    };


    $scope.selectAll = function () {
        for (var i = 0; i < $scope.MusicList.length; i++) {
            $scope.MusicList[i].isChecked = $scope.allItemsSelected;
        }
    };

    //MODAL ADD PLAYLIST
    $scope.AddPlayListModal = function () {
        $scope.arrayMusicID = [];
        var counter = 0;
        //how many checkbox is checked
        for (var i = 0; i < $scope.MusicList.length; i++) {
            if ($scope.MusicList[i].isChecked) {
                $scope.arrayMusicID.push($scope.MusicList[i].MusicID);
                counter++;
            }

        }
        if (counter == 0) {
            alertify.warning("Choose at least one music before add playlist");
            return;
        }

        else {
            var modalInstance = $modal.open({
                templateUrl: 'Templates/AddPlayListModal.html',
                controller: 'AddPlayListModalController',
                resolve: {
                    arraySelMusic: function () {
                        return $scope.arrayMusicID;
                    }
                }
            });
        }
    };
    
}]);


////MODAL////
myapp.controller('AddMusicModalController', ['$scope', '$http', '$modalInstance', 'test', function ($scope, $http, $modalInstance, test) {

    $scope.input = {};

    $scope.MusicList = test;   // for updating/push table content     

    $scope.ok = function () {

            var postData = new Object();
            postData.Name = $scope.input.musicName;
            postData.Title = $scope.input.musicTitle;
            postData.Album = $scope.input.musicAlbum;
            postData.Genre = $scope.input.musicGenre;


            $http({
                method: 'POST',
                url: '/Data/AddMP3',
                data: postData
            })
                .success(function (data) {
                    $scope.MusicList.push(data);

               })

               .error(function (data) {
                   alertify.error("Error with adding new music");                
               });

            $modalInstance.close();
    };

    $scope.cancel = function () {

        $modalInstance.dismiss('cancel');

    };

}]);

myapp.controller('EditMusicModalController', ['$scope', '$http', '$modalInstance', 'selectedMusic', 'AllMusic', function ($scope, $http, $modalInstance, selectedMusic, AllMusic) {

    $scope.MusicList = AllMusic;
    $scope.MusicSel = {
        Name: selectedMusic.Name,
        Title: selectedMusic.Title,
        Album: selectedMusic.Album,
        Genre: selectedMusic.Genre,
    };

    var id = selectedMusic.MusicID;

    $scope.ok = function () {

        var putData = new Object();
        putData.Name = $scope.MusicSel.Name;
        putData.Title = $scope.MusicSel.Title;
        putData.Album = $scope.MusicSel.Album;
        putData.Genre = $scope.MusicSel.Genre;
        $http({
            method: 'PUT',
            url: '/Data/EditMP3/' + id,
            data: putData
        })
            .success(function (data, status, headers, config) {

                if (data != null && data != "") {

                    for (var i = 0; i < $scope.MusicList.length; i++) {
                        if ($scope.MusicList[i].MusicID === id) {
                            $scope.MusicList.splice(i, 1);

                            $scope.MusicList.push(data);
                        }
                    }
                }

            })

            .error(function (data, status, headers, config) {
                alertify.error("Error with edit music");
            });

            $modalInstance.close();

    }


    $scope.cancel = function () {

        $modalInstance.dismiss('cancel');

    };
}]);


///////////////////////////////////////////////////////////////////////////////////////////////

myapp.controller('PlayListController', ['$scope', '$http', '$stateParams', '$location', function ($scope, $http, $stateParams, $location) {

    $scope.PlayList = null;

    //GET
    $http({
        method: 'GET',
        url: "/Data/GetPlayList",
    })
      .success(function (data) {
          $scope.PlayList = data;

      })
      .error(function (data) {
          alertify.error("Error getting playlist");
      });

    //Search
    $scope.SearchPlayList = function () {

        $http({
            method: 'GET',
            url: "/Data/FilterPlayList",
            params: { inputSearchPlayList: $scope.input.SearchPlayList }
            })
            .success(function (data) {
                $scope.PlayList = data;
            })
            .error(function (data) {
                alertify.error("Error with filtering playlist");
            });
    }

    //DELETE
    $scope.deletePlayList = function (id) {

        alertify.dialog('confirm')
           .show()
           .set({ title: 'Delete playlist' })                   
           .set('defaultFocus', 'cancel')    
           .set({
               'labels': { ok: 'Ok', cancel: 'Cancel' },
               'message': 'Are you sure you want to delete?',
               'onok': function () {

                   $http({
                       method: 'DELETE',
                       url: '/Data/DeletePlayList/' + id,
                   })

                   .success(function (data) {
                       for (var i = 0; i < $scope.PlayList.length; i++) {
                           if ($scope.PlayList[i].PlayListID === id) {
                               $scope.PlayList.splice(i, 1);
                           }
                       }

                       $("#PlaylistDetails1").empty();

                   })
                   .error(function (data) {
                       alertify.error("Error with number id =" + id);
                   });
               },
               'oncancel': function () { }
           });
    };

    

    //details
    $scope.PlayListDetails = function (selPlayList) {

        $scope.MusicPlayList = {};

        $scope.PlayListName = "";

        //GET MUSIC BY PLAYLIST ID
        $http({
            method: 'GET',
            url: "/Data/GetMusicByPlayList",
            params: { playlistID: selPlayList.PlayListID }
        })
          .success(function (data) {
              $scope.MusicPlayList = data;
          })
          .error(function (data) {
              alertify.error("Error getting music playlist ");
          });
    
    };

}]);

////MODAL////
myapp.controller('AddPlayListModalController', ['$scope', '$http', '$modalInstance', 'arraySelMusic', function ($scope, $http, $modalInstance, arraySelMusic) {

    $scope.PlayListName = {
        Name: ''
    };
 

    $scope.ok = function () {

        $http({
            method: 'POST',
            url: '/Data/AddPlayList',
            data: { name: $scope.PlayListName.Name, arraySelMusic: String(arraySelMusic) }
        })
            .success(function (data) {
                alertify.success("PlayList is created");
                
            })

           .error(function (data) {
               alertify.error("Error with adding new playlist");
            });

        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');

    };

}]);


myapp.controller('EditPlayListNameController', ['$scope', '$http', '$stateParams', function ($scope, $http, $stateParams ) {

    if ($stateParams.id != "") {
        // GET MUSIC BY PLAYLIST ID
        $http({
            method: 'GET',
            url: "/Data/GetMusicByPlayList",
            params: { playlistID: $stateParams.id }
        })
          .success(function (data) {
              $scope.MusicPlayList = data[0];
              if (data[1] != null) {
                  $scope.PlayListName = data[1].Name;
              }
              
          })
          .error(function (data) {
              alertify.error("Error getting music playlist ");
          });

    }
    else {
        alertify.error("Error unknown playlist ID ");
    }

    $scope.change = function (newValue) {
        $scope.changedName = newValue;
    }

    $scope.ok = function () {
 
        //only if name is changed, function will continue
        if ($scope.changedName != undefined) {

            $http({
                method: 'PUT',
                url: '/Data/EditPlayListName/',
                params: { playlistID: $stateParams.id, playlistName: $scope.changedName }
            })
                .success(function (data, status, headers, config) {
                    if (data != null && data != "") {
                        
                        for (var i = 0; i < $scope.PlayList.length; i++) {
                            if ($scope.PlayList[i].PlayListID == $stateParams.id) {
                                
                                $scope.PlayList.splice(i, 1);                             
                                $scope.PlayList.push(data);
                                alertify.success("Playlist name is changed");
                            }
                        }
                    }

                })

                .error(function (data, status, headers, config) {
                    alertify.error("Error with edit playlist name");
                });
        }

    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');

    };
}]);
