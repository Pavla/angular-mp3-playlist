﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace MP3.Controllers
{
    public class DataController : Controller
    {

        MP3DatabaseEntities1 db = new MP3DatabaseEntities1();

        ///////////////////////////MP3 LIST///////////////////////////

        // GET MUSIC: /Data/
        public JsonResult GetMP3List()
        {

            var MP3List = (from p in db.Music orderby p.Name ascending select p).ToList();

            return Json(MP3List, JsonRequestBehavior.AllowGet);

        }

        // DELETE MUSIC: /Data/
        public void DeleteMP3(int id)
        {
            var musicID = (from p in db.Music where p.MusicID == id select p).FirstOrDefault();

            if (musicID != null)
            {
                db.Music.Remove(musicID);
                db.SaveChanges();
            }

            var manageList = (from p in db.ManageList where p.MusicID == id select p).ToList();
            if (manageList.Count > 0)
            {
                foreach (var item in manageList)
                {
                    db.ManageList.Remove(item);
                    
                }
                db.SaveChanges();               
            }

        }


        // ADD MUSIC: /Data/
        public JsonResult AddMP3(Music postdata)
        {
            db.Music.Add(postdata);
            db.SaveChanges();

            return Json(postdata, JsonRequestBehavior.AllowGet);
        }

        // EDIT MUSIC: /Data/
        public JsonResult EditMP3(int id, Music putData)
        {

            var Music = (from p in db.Music where p.MusicID == id select p).FirstOrDefault();

            if (Music != null)
            {
                Music.Name = putData.Name;
                Music.Title = putData.Title;
                Music.Album = putData.Album;
                Music.Genre = putData.Genre;
                db.SaveChanges();
            }

            return Json(Music, JsonRequestBehavior.AllowGet);

        }

        
        // FILTER MUSIC: /Data/
        public JsonResult FilterMP3List(string singleSelectMusic, string inputSearchMusic)
        {
            var FilterMP3List = new List<MP3.Music>();

            if (singleSelectMusic == "Name")
            {
                FilterMP3List = (from p in db.Music where p.Name.Contains(inputSearchMusic) orderby p.Name ascending select p).ToList();         
            }
            else if (singleSelectMusic == "Title")
            {
                FilterMP3List = (from p in db.Music where p.Title.Contains(inputSearchMusic) orderby p.Name ascending select p).ToList();
            }

            else if (singleSelectMusic == "Album")
            {
                FilterMP3List = (from p in db.Music where p.Album.Contains(inputSearchMusic) orderby p.Name ascending select p).ToList();
            }

            else if (singleSelectMusic == "Genre")
            {
                FilterMP3List = (from p in db.Music where p.Genre.Contains(inputSearchMusic) orderby p.Name ascending select p).ToList();
            }

            else
            {
                FilterMP3List = (from p in db.Music orderby p.Name ascending select p).ToList();
                
            }

            return Json(FilterMP3List, JsonRequestBehavior.AllowGet);

        }

        //////////////////////////////PLAYLIST//////////////////////////////////////////////

        // GET PLAYLIST: /Data/
        public JsonResult GetPlayList()       
        {
            var PlayList = (from p in db.Playlist orderby p.Name ascending select p).ToList();
            return Json(PlayList, JsonRequestBehavior.AllowGet);

        }

        public void AddPlayList(string name, string arraySelMusic)       
        {
            //ADD PLAYLIST
            Playlist playlist = new Playlist();
            playlist.Name = name;
            db.Playlist.Add(playlist);
            db.SaveChanges();
            

            //ADD MANGAGELIST

            var lastIDPlayList = (from p in db.Playlist orderby p.PlayListID descending select p.PlayListID).FirstOrDefault();

            if (arraySelMusic != "")
            {
                string[] IDs = arraySelMusic.Split(',');

                foreach (string id1 in IDs)
                {
                    int ID = Convert.ToInt32(id1);

                    ManageList manageList = new ManageList();
                    manageList.PlayListID = lastIDPlayList;
                    manageList.MusicID = ID;
                    db.ManageList.Add(manageList);
                }
            }

            db.SaveChanges();
        }

        // DELETE PlayList: /Data/
        public void DeletePlayList(int id)
        {
            //Manage List

            var ManagePlayLists = (from p in db.ManageList where p.PlayListID == id select p).ToList();

            if (ManagePlayLists.Count > 0)
            {
                foreach (var item in ManagePlayLists)
                {
                    db.ManageList.Remove(item);                   
                }        
            }
            db.SaveChanges();


            var PlayListID = (from p in db.Playlist where p.PlayListID == id select p).FirstOrDefault();

            if (PlayListID != null)
            {
                db.Playlist.Remove(PlayListID);
                db.SaveChanges();
            }

        }

        // EDIT PLAYLIST: /Data/
        public JsonResult EditPlayListName(int playlistID, string playlistName)
        {
            var PlayL = (from p in db.Playlist where p.PlayListID == playlistID select p).FirstOrDefault();

            if (PlayL != null)
            {
                PlayL.Name = playlistName;
                db.SaveChanges();
            }

            return Json(PlayL, JsonRequestBehavior.AllowGet);
        }
        

        // GET MUSIC in PlayLIst: /Data/
        public JsonResult GetMusicByPlayList(int playlistID)
        {
            List<object> resp = new List<object>();
            
            List<object> musics = new List<object>();
            resp.Add(musics);

            var PlaylistName = (from p in db.Playlist where p.PlayListID == playlistID select p).FirstOrDefault();

            resp.Add(PlaylistName);

            var ManageList = (from p in db.ManageList where p.PlayListID == playlistID select p).ToList();
            foreach (var item in ManageList)
            {
                var MusicDetail = (from p in db.Music where p.MusicID == item.MusicID select p).FirstOrDefault();
                musics.Add(MusicDetail);
            }
            return Json(resp, JsonRequestBehavior.AllowGet);

        }


        // FILTER PlayList: /Data/
        public JsonResult FilterPlayList(string inputSearchPlayList)
        {
            var FilterPlayList = (from p in db.Playlist where p.Name.Contains(inputSearchPlayList) orderby p.Name ascending select p).ToList();

            return Json(FilterPlayList, JsonRequestBehavior.AllowGet);

        }

       
    }
}
